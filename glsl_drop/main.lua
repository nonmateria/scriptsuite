
require "strict"

----------------------------------------------------
local w = args.get_number( "-w", 480) 
local h = args.get_number( "-h", 480)
local m = args.get_number( "-m", 1)

if m < 1 then m = 1 end
window.size( w*m, h*m )

if args.count() > 0 then 
	-- TODO: compare arg to ".frag" here 
    frag.load( args.get(0), "glsl" ) 
	title =  args.get(1)
    window.title( title )
else
	window.quit()
end

layer.create( "def", w, h, m )
if args.get_bool( "--lin" ) then 
	layer.filter_linear()
end 

local px = args.get_number( "-x", -1)
local py = args.get_number( "-y", -1)

if px >= 0 and py >= 0 then 
	window.move( px, py )
end

png.align_center()
png.load( "tests" )

local blackground = true
local clock = 60

----------------------------------------------------
function loop()
    frag.apply( "glsl" )

	png.select( "tests" );

	if clock == 60 then 
		if blackground then 
	        layer.clear( 0, 0, 0, 255 )
	        gfx.color( 255, 255, 255, 255 ) 
	        png.draw( w*0.5, h*0.5 )
	        png.next()
		else 
	        layer.clear( 255, 255, 255, 255 )
	        gfx.color( 0, 0, 0, 255 ) 
	        png.draw( w*0.5, h*0.5 )
	        png.next()
		end 
		clock = 0
	end 

	clock = clock + 1 

	if key.pressed( key.space ) then 
		blackground = not blackground;
	end
end

