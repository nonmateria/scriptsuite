
require "strict"

--[[-- KEYS -----------------------
	wasd/arrows = move tile
	enter/c = save tile 
	
--]]-- OPTIONS --------------------

---[[ -- zine page
local tilew = 3508
local tileh = 4961
local scale = 0.15
--]]

--[[ -- A4 300 DPI	
local tilew = 2480
local tileh = 3508
local scale = 0.2
--]]

--[[ -- A5 300 DPI
local tilew = 1748
local tileh = 2480
local scale = 0.25
--]]

local inputpath = args.get_string( "-i", "input.png" )
local outputpath = args.get_string( "-o", "" )

----------------------------------------------------
-- TODO make input file an optional argument 
-- TODO exit when no input is given 

local numsaved = 0
local cx = 0
local cy = 0

window.size( 640, 480 )
window.title( "zinecutter" )

png.load( inputpath, "input" ) 
png.select( "input" )

window.size( png.width()*scale + 40, png.height()*scale + 20 )
layer.create( "bg" )
	layer.clear( 10, 15, 15, 255 )
layer.create( "image", png.width()*scale + 20 + tilew, png.height()*scale ) 
layer.create( "cut", tilew, tileh )
	layer.hide()
	
----------------------------------------------------
function loop()
	png.select( "input" )
	png.align_corner() 

	layer.select("cut") ----------------
	gfx.color( 255, 255, 255, 255 )
	--px.clear( 255, 0, 0, 255 )
	png.draw( -cx, -cy ) 

	layer.select("image") ----------------
	layer.clear( 0, 0, 0, 0 )
	gfx.push()
		gfx.scale( scale, scale ) 
		png.draw( 0, 0 ) 
		overlay( cx, cy, tilew, tileh )
	gfx.pop()

	gfx.push()
		gfx.translate( png.width()*scale + 20, 20 )
		gfx.color(255, 255, 255, 255)
		layer.draw( "cut", 0, 0 )
		overlay( 0, 0, tilew, tileh )
	gfx.pop()

	key_pressed()
end

----------------------------------------------------
function overlay( x, y, w, h )
	gfx.push()
		gfx.color( 255, 0, 0, 255 )
		gfx.translate( x, y ) 
		gfx.rect( 1, 1, w-1, h-1 )
		gfx.line( w/2, 0, w/2, h ) 
		gfx.line( 0, h/2, w, h/2 ) 
	gfx.pop()
end

function key_pressed()
	local faststep = 4
	
	if key.down( key.a ) then
		cx = cx-faststep
	elseif key.down( key.d ) then
		cx = cx+faststep
	elseif key.down( key.s ) then
		cy = cy+faststep
	elseif key.down( key.w ) then
		cy = cy-faststep
	elseif key.pressed( key.left_arrow ) then
		cx = cx-1
	elseif key.pressed( key.right_arrow ) then
		cx = cx+1
	elseif key.pressed( key.up_arrow ) then
		cy = cy-1
	elseif key.pressed( key.down_arrow ) then
		cy = cy+1
	elseif key.pressed( key.c ) or key.pressed( key.enter ) then
		local filename = outputpath.."frame_"..tostring(numsaved)..".png"
		layer.select("cut")
		layer.save( filename )
		numsaved = numsaved + 1
		print( filename )
	end
end
