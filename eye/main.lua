
require "strict"

--[[-- KEYS -----------------------
	space = saves a frame 
	
--]]-- OPTIONS --------------------
local camw = args.get_number( "-w", 640 )
local camh = args.get_number( "-h", 480 ) 
local device = args.get_string( "-d", "/dev/video0" )

----------------------------------------------------
local numsaved = 0
local freeze = 0

window.size( camw, camh)
window.title( "eye" )
layer.create("def")

cam.open(device, camw, camh )

frag.load( "process.frag" ) 

local bProcess = true

----------------------------------------------------
function loop()
	freeze = freeze + 1
	if freeze > 0 then 	
    	layer.clear(0,0,0,0)
		cam.draw( 0, 0 )

	   	if bProcess then 
	   		frag.apply( "process" )
	   	end
	end

	if key.pressed( key.space ) then
		local filename = "output_"..tostring(numsaved)..".png"
		layer.select("def")
		layer.save( filename )
		freeze = -40
		numsaved = numsaved + 1
		print( filename )
	end

	if key.pressed( key.p ) then 
		bProcess = not bProcess
	end 
end
