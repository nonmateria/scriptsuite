
require "strict"

----------------------------------------------------
local w = args.get_number( "-w", 480) 
local h = args.get_number( "-h", 480)
local m = args.get_number( "-m", 1)
local t = args.get_number( "-t", 1.0)

if m < 1 then m = 1 end
window.size( w*m, h*m )

if args.count() > 0 then 
	-- TODO: compare arg to ".frag" here 
    frag.load( args.get(0), "glsl" ) 
	title =  args.get(1)
    window.title( title )
else
	window.quit()
end
  
layer.create( "def", w, h, m )
if args.get_bool( "--lin" ) then 
	layer.filter_linear()
end 

local px = args.get_number( "-x", -1)
local py = args.get_number( "-y", -1)

if px >= 0 and py >= 0 then 
	window.move( px, py )
end

clock.warp( t );

----------------------------------------------------
function loop()
	-- activate to know fps 
	--window.title( window.get_framerate().."f | "..title )

	layer.select("def")
    frag.apply( "glsl" )

	if key.pressed( key.s ) then 
		window.save("output.png")
		print ( "saved to output.png" )
	end 
end
