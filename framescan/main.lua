
require "strict"

--[[-- KEYS -----------------------
	1234 : preview modes 
	5 : onion overlay
	q/l/spacebar : lock tile 
	wasd/arrows : move tile
	e/enter = add tile to frames and save

------ GAMEPAD --------------------
	x : lock tile
	o : add and save
	tri: activate onion overlay
	R1/R2: visualize previous/next frame
	right axis / arrows : calibrate lock 
	
--]]-- OPTIONS --------------------

-- sony dual shock 4
ds4 = { x=7, s=8, o=6, t=5,
		up=1, down=3, left=4, right=2,
		L1=9, L2=10, R1=11, R2=12, share=13, options=15, 
		left_axis_x=0, left_axis_y=1, right_axis_x=2, right_axis_y=3,
		left_trigger=4, right_trigger=5,
		left_axis=16, right_axis=17 }

-- acquisition
local tile_w = 400
local tile_h = 400
local lo = 0.15  
local hi = 0.5

-- camera
local cam_dev = "/dev/video2"    
local cam_w = 1280 
local cam_h = 720
local cam_rotate = false

-- general 
local path = "output/" 
local scale = 1.0

-- guides function
function draw_guides( x, y, w, h )
	gfx.push()
		gfx.color( 255,0,0, 255 )
		gfx.translate(x,y) 
		
		-- border
		gfx.rect( 1, 1, w-1, h-1 )
		-- cross to center cp 
		gfx.line( w/2, 0, w/2, h ) 
		gfx.line( 0, h/2, w, h/2 ) 

		-- add here
		--local bottom = h*0.7
		--gfx.line( 0, bottom, w, bottom ) 
		local eye = h*0.4
		gfx.circle_fill( w*0.5, eye, 6 )

	gfx.pop()
end

----------------------------------------------------
local numsaved = 0
local cx = 0
local cy = 0
local preview = 0
local framemem = 0
local clock = 0
local draw_guides_preview = false
local draw_guides_index = 0

cx = (cam_w-tile_w) * 0.5
cy = (cam_h-tile_h) * 0.5

local canvas_h = cam_h*scale 
local tile2 = tile_h*2 + 40 
if tile2>canvas_h then 
	canvas_h = tile2
end

local winh =  cam_h*scale + 20 
if tile_h * 2 > winh then 
	winh = tile_h * 2 + 60
end

window.size( cam_w*scale + 60 + tile_w, winh )
window.title( "framescan" )

layer.create( "default", cam_w*scale + 40 + tile_w, canvas_h ) 
layer.create( "cam", cam_w, cam_h ) 
	layer.hide()
layer.create( "locked", cam_w, cam_h) 
	layer.hide()
	local cut_offset_x = 0
	local cut_offset_y = 0 
layer.create( "cut", tile_w, tile_h )
	layer.hide()

frag.load( "process.frag" ) 

png.load( path, "output" )
png.select("output")
numsaved = png.size()

if cam_rotate then
	local temp = cam_w 
	cam_w = cam_h
	cam_h = temp
	cam.open( cam_dev, cam_h, cam_w)
else
	cam.open( cam_dev, cam_w, cam_h)
end

----------------------------------------------------
function loop()
	png.align_corner() 
	png.select( "output" )	

	layer.select("cam")
	gfx.color( 255, 255, 255, 255 )
	if cam_rotate then
		gfx.translate( cam_w, 0 ) 
		gfx.rotate( math.pi * 0.5 )
	end 
	cam.draw( 0, 0 )
	
    frag.select( "process" )
        frag.uniform( "u_low", lo )
        frag.uniform( "u_high", hi)
    frag.apply( "process") 

	controls_pt1()
	
	layer.select("cut")
	gfx.push()
		layer.clear( 0, 0, 0, 0 )
		gfx.color( 255, 255, 255, 255 )
		layer.draw( "locked", -cx + cut_offset_x, -cy + cut_offset_y )	
	gfx.pop()

	layer.select("default") ----------------
		layer.clear( 10, 15, 15, 255 )
		gfx.push()
		gfx.scale( scale, scale ) 
			layer.draw( "cam", 0, 0 )
			draw_guides( cx, cy, tile_w, tile_h )

			if draw_guides_preview then 
				if draw_guides_index == 0 then
					png.color(255, 0, 0, 180)
					local prev = png.size()-2
					if prev < 0 then prev = 0 end 
					png.frame( prev ) 
					png.draw( cx, cy )
				else
					png.color(255, 0, 0, 180)
					png.frame( 0 )
					png.draw( cx, cy )
				end
			end
		gfx.pop()

		gfx.push()
		gfx.translate( cam_w*scale + 20, 20 )

		png.color(255,255,255, 255)

		if png.size() ~= 0 then
			if preview == 0 then 
				layer.draw( "cut", 0, 0 )
			elseif preview == 1 then
				local prev = png.size()-2
				if prev < 0 then prev = 0 end 
				png.frame( prev ) 
				png.draw( 0, 0 )	
			elseif preview == 2 then 
				png.frame( 0 )
				png.draw( 0, 0 )
			elseif preview == 3 then 
				layer.draw( "cut", 0, 0 )
				gfx.blend_additive()
				gfx.color(255,0,0, 255)
				png.frame( 0 )
				png.draw( 0, 0 )
			elseif preview == 4 then
				layer.draw( "cut", 0, 0 )
				png.frame( png.size()-1 )
				gfx.blend_additive()
				gfx.color(255,0,0, 255)
				png.draw( 0, 0 )
			end
		else
			layer.draw( "cut", 0, 0 )
		end
		
		draw_guides( 0, 0, tile_w, tile_h )
		
		gfx.blend_alpha()

		gfx.push()
			gfx.translate( 0, tile_h+20 )
			gfx.color(255,255,255, 255)

			if clock == 0 then 
				framemem = framemem + 1 
				if framemem > png.size() then 
					framemem = 0
				end				
				clock = 10
			end
			clock = clock - 1

			if framemem == png.size() then 
				layer.draw( "cut", 0, 0 )
			else 
				png.frame( framemem )
				png.draw( 0, 0 )
			end 
			
		gfx.pop()		
	gfx.pop()

	controls_pt2()
end

----------------------------------------------------

function controls_pt1()
	if key.pressed( key.q ) 
	or key.pressed( key.l ) 
	or key.pressed( key.space ) 
	or pad.pressed( ds4.x ) then
		layer.select("locked")
		layer.clear( 0, 0, 0, 0 )
		gfx.color( 255, 255, 255, 255 )
		layer.draw( "cam", 0, 0 )
		cut_offset_x = 0
		cut_offset_y = 0
	end
end

function controls_pt2()
	if key.pressed( key.e ) 
	or key.pressed( key.enter )
	or key.pressed( key.c )
	or pad.pressed( ds4.o ) then
		local filename = path.."frame_"
		if numsaved<10 then 
			filename = filename.."00"
		elseif numsaved<100 then 
			filename = filename.."0"
		end
		filename = filename..tostring(numsaved)..".png"
		layer.select("cut")
		layer.save( filename )
		png.load( path, "output" )
		numsaved = numsaved + 1 
	end

	if key.down( key.n1 ) 
	or pad.down( ds4.R1 ) then
		preview = 1
		draw_guides_index = 0
	elseif key.down( key.n2 )
	or pad.down( ds4.L1 ) 
	or pad.down( ds4.R2 ) then
		preview = 2
		draw_guides_index = 1
	elseif key.down( key.n3 ) then
		preview = 3
	elseif key.down( key.n4 ) then
		preview = 4
	else 
		preview = 0
	end

	if key.pressed( key.n5 ) or pad.pressed( ds4.t) then
		draw_guides_preview = not draw_guides_preview  
	end

	local faststep = 4
	if key.down( key.a ) then
		cut_offset_x = cut_offset_x-1
	elseif key.down( key.d ) then
		cut_offset_x = cut_offset_x+1
	elseif key.down( key.s ) then
		cut_offset_y = cut_offset_y+1
	elseif key.down( key.w ) then
		cut_offset_y = cut_offset_y-1
	elseif key.pressed( key.left_arrow ) or pad.pressed( ds4.left ) then
		cut_offset_x = cut_offset_x-1
	elseif key.pressed( key.right_arrow ) or pad.pressed( ds4.right ) then
		cut_offset_x = cut_offset_x+1
	elseif key.pressed( key.up_arrow ) or pad.pressed( ds4.up ) then
		cut_offset_y = cut_offset_y-1
	elseif key.pressed( key.down_arrow ) or pad.pressed( ds4.down ) then
		cut_offset_y = cut_offset_y+1
	end 

	local js_amount = 2
	local dx = pad.axis( ds4.right_axis_x ) * js_amount
	local dy = pad.axis( ds4.right_axis_y ) * js_amount
	cut_offset_x = cut_offset_x-dx
	cut_offset_y = cut_offset_y-dy
end
