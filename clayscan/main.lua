
require "strict"

--[[-- KEYS -----------------------
	1/2 = change red frame overlay 
	3 = deactivate overlay 
	space = lock 
	enter = acquire 
	
--]]-- OPTIONS --------------------

-- camera
local camdev = "/dev/video2" 
local camw = 640 
local camh = 480
local cam_rotate = true

-- acquisition 
local use_black_clay = false
local lo = 0.2  
local hi = 0.4 
local tilew = 360
local tileh = 360

-- general
local scale = 1.5
local path = "output/" 

----------------------------------------------------
local numsaved = 0
local cx = 0
local cy = 0
local lock_cam = false
local preview = 0
local framemem = 0
local clock = 0
local overlay_preview = 0
local canvasw = camw*scale + 60 + tilew
local canvash = camh*scale + 40

window.size( canvasw, canvash )
window.title( "framescan" )

if cam_rotate then
	local temp = camw 
	camw = camh
	camh = temp
	cam.open( camdev, camh, camw)
else
	cam.open( camdev, camw, camh)
end

cx = (camw-tilew) * 0.5
cy = (camh-tileh) * 0.5

if use_black_clay then 
	frag.load( "black.frag", "process" ) 
else
	frag.load( "process.frag" ) 
end

png.load( path, "output" )
png.select("output")
numsaved = png.size()   

layer.create( "default", canvasw, canvash ) 
layer.create( "cam", camw, camh ) 
	layer.hide()
layer.create( "locked", camw, camh) 
	layer.hide()
	local cut_offset_x = 0
	local cut_offset_y = 0 
layer.create( "cut", tilew, tileh )
	layer.hide()

----------------------------------------------------
function loop()
	png.align_corner() 
	png.select( "output" )	

	layer.select("cam")
	gfx.color( 255, 255, 255, 255 )
	if cam_rotate then
		gfx.translate( camw, 0 ) 
		gfx.rotate( math.pi * 0.5 )
	end 
	cam.draw( 0, 0 )
	
    frag.select( "process" )
        frag.uniform( "u_low", lo )
        frag.uniform( "u_high", hi )
    frag.apply( "process") 

	if key.pressed( key.space ) then
		layer.select("locked")
		layer.clear( 0, 0, 0, 0 )
		gfx.color( 255, 255, 255, 255 )
		layer.draw( "cam", 0, 0 )
		cut_offset_x = 0
		cut_offset_y = 0
	end

	layer.select("cut")
	gfx.push()
		layer.clear( 0, 0, 0, 0 )
		gfx.color( 255, 255, 255, 255 )
		layer.draw( "locked", -cx + cut_offset_x, -cy + cut_offset_y )	
	gfx.pop()

	layer.select("default") ----------------
	if use_black_clay then 
		layer.clear( 255, 255, 255, 255 )
	else
		layer.clear( 10, 15, 15, 255 )
	end
	
	gfx.translate( 0, 20 ) 
			
	gfx.push()
	gfx.scale( scale, scale ) 
		layer.draw( "cam", 0, 0 )
		overlay( cx, cy, tilew, tileh )
	
		if overlay_preview == 1 then		
			gfx.blend_additive()
			gfx.color(255, 0, 0, 150)
			png.frame( numsaved-1 )	
			png.draw( cx, cy )
		elseif overlay_preview == 2 then
			gfx.blend_additive()
			gfx.color(255, 0, 0, 150)
			png.frame( 0 )
			png.draw( cx, cy )
		end
	gfx.pop()

	gfx.push()
		gfx.translate( camw*scale + 20 , 20 )

		gfx.color(255,255,255, 255)

		gfx.blend_alpha()

		gfx.push()
			gfx.color(255,255,255, 255)

			if clock == 0 then 
				framemem = framemem + 1 
				if framemem > numsaved then 
					framemem = 0
				end				
				clock = 8  
			end
			clock = clock - 1

			if framemem == png.size() or framemem == numsaved then 
				layer.draw( "cut", 0, 0 )
			else 
				png.frame( framemem )
				png.draw( 0, 0 )
			end 

			gfx.color( 255, 0, 0, 255 ) 
			gfx.rect( 1, 1, tilew, tileh )
		gfx.pop()		
	gfx.pop()

	controls()
end

----------------------------------------------------
function overlay( x, y, w, h )
	gfx.push()
		gfx.color( 255,0,0, 255 )
		gfx.translate(x,y) 
		gfx.rect( 1, 1, w-1, h-1 )
		gfx.line( w/2, 0, w/2, h ) 
		gfx.line( 0, h/2, w, h/2 ) 
	gfx.pop()
end

function controls()
	if key.pressed( key.enter ) then
		local filename = path
		filename = filename.."frame_"
		if numsaved<10 then 
			filename = filename.."00"
		elseif numsaved<100 then 
			filename = filename.."0"
		end
		filename = filename..tostring(numsaved)..".png"
		layer.select("cut")
		layer.save( filename )
		png.load( path, "output" )
		print( "saved "..filename.." in slot "..numsaved )
		numsaved = numsaved + 1 
	end

	if key.pressed( key.backspace ) and numsaved>0 then
		numsaved = numsaved - 1 
		print( "removed frame ".. numsaved ) 
	end 

	if key.down( key.n1 ) then
		overlay_preview = 1
	elseif key.down( key.n2 ) then
		overlay_preview = 2 
	elseif key.down( key.n3 ) 
	or key.down( key.n4 )  
	or key.down( key.n6 )  
	or key.down( key.n0 ) then
		overlay_preview = 0
	end
end
