
require "strict"

--[[-- KEYS -----------------------
	space = start / stop saving
	
--]]-- OPTIONS --------------------
local camw = 640
local camh = 480 
local camdev = "/dev/video0"
local cam_rotate = false

local w = 640
local h = 480

----------------------------------------------------
window.size( w, h )
window.title( "eye" )
layer.create("def")

layer.create("over")
	layer.clear( 0, 0, 0, 0 ) 
--[[ -- calibration lines
  	gfx.color(255, 0, 0, 255)
  	local x =layer.width() * 0.5
  	local y = layer.height() * 0.22	
  	--local y = 430
  	gfx.line( x, 0, x, layer.height() ) 
  	gfx.line( 0, y, layer.width(), y )
  	local rw = 240
  	local rh = rw*3/4
  	local rx = x-rw/2
  	local ry = y-rh/2
	--print ( "coo "..rx.." "..ry.." ".." "..rw.." "..rh)
  	gfx.rect( rx, ry, rw, rh)
--]]

cam.open(camdev, camw, camh )

frag.code( [[
	#ifdef GL_ES
	precision highp float;
	#endif
	
	uniform vec2 u_resolution;
	uniform float u_time;
	uniform sampler2D u_tex_fb;

	void main(){
	    vec2 st = gl_FragCoord.xy/u_resolution;
	    
	    //st.x = 1.0-st.x; // flip horizontally 
	    //st.y = 1.0-st.y;  // flip vertically 

	    vec4 source = texture2D( u_tex_fb, st ); // for texture access
	    vec3 color = source.rgb;
	    float luminance = color.r*0.299 + color.g*0.587 + color.b*0.114;	

	    float low = 0.25;
	    float high = 0.5;
	    
	    float alpha = smoothstep( low, high, luminance );
	    
	    gl_FragColor = vec4( vec3(alpha), 1.0 );
	}
]], "process" ) 

----------------------------------------------------
local enabled = false
local numsaved = 0
local freeze = 0

function loop()
	layer.select( "def")
	freeze = freeze + 1
	if freeze > 0 then
    	layer.clear(0,0,0,0)
    	if cam_rotate then
			gfx.translate( camh, 0 ) 
			gfx.rotate( math.pi * 0.5 )
			cam.draw( 0, 0 )
    	else
			cam.draw( -(camw-w)/2, 0 )
		end
		
	   	frag.apply( "process" )
	end


	
	if key.pressed(key.space) then
		enabled = not enabled
	end

	if enabled and clock.frame()%90 == 0 then
		local numstring = ""..tostring(numsaved) 
		if( numsaved < 10 ) then
			numstring = "00"..numstring
		elseif (numsaved < 100 ) then
			numstring = "0"..numstring
		end
		local filename = "output_"..numstring..".png"
		layer.select("def")
		layer.save( filename )
		freeze = -15
		numsaved = numsaved + 1
		print( filename )
	end
end
