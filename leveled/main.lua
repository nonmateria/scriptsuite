
require "strict"
json = require "json"

local TILE = 60
local cells_w = 20 
local cells_h = 40 
local tileset = "tiles.png"
local levelfile = "map.data"

local map_iw = 20
local map_ih = 15
local map_scale = 1.0

----------------------------------------------------
local map_ix = 0
local map_iy = 0 
local map_w = map_iw * TILE
local map_h = map_ih * TILE

local mw = cells_w * TILE
local mh = map_ih * TILE

local tix = 0
local tiy = 0

window.size( 1, 1 )

png.load( tileset )

window.size( png.width() + mw*map_scale, mh*map_scale )

layer.create( "def")
layer.create( "map", mw, mh) 
	layer.position( png.width(), 0 ) 
	layer.scale( map_scale ) 


----------------------------------------------------
local data = {}
data.width = cells_w
data.height = cells_h
data.cells = {}
-- a single dimentional file is easier to parse for c
-- it has to start from 1 because json.lua doesn't like 0-indexed arrays
local datasize = cells_w * cells_h
for n=1,datasize do
	data.cells[n] = { ix = 0, iy = 0 } -- empty cell 
end

function update_map()
	layer.select( "map" )
	layer.scale( map_scale ) 
	
	layer.clear( 0, 0, 0, 0 ) 
	gfx.color( 255, 20, 0, 255 )
	png.color( 0, 0, 0, 255 ) 

	for yi=0,map_ih-1 do 
		for xi=0,map_iw-1 do 
			local xii = xi + map_ix
			local yii = yi + map_iy
			local ax = data.cells[1 + yii*cells_w + xii].ix * TILE
			local ay = data.cells[1 + yii*cells_w + xii].iy * TILE
			
			png.sub( xi*TILE, yi*TILE, ax, ay, TILE, TILE ) 
		end
	end	 
end

-- try to read file 
local file = io.open(levelfile, "r")
if file then 
	io.input( file ) 
	data = json.decode( io.read() )
	--print( io.read() ) 
	io.close( file )
	update_map()
else 
	print( "[leveled] no file "..levelfile.." found" ) 
end 


----------------------------------------------------
function loop()

	shortcuts()

	if mouse.pressed( mouse.left ) then
		check_tilecontrol()
		check_mapcontrol()
	end

	layer.select( "def" ) 
	layer.clear(255, 255, 255 ,255)
	png.color( 0, 0, 0, 255 ) 
	gfx.color( 255, 0, 0, 255 )

	--local tx = math.floor( fn.triangle(clock.get()*0.2) * 5 ) * TILE 
	local tx = tix * TILE
	local ty = tiy * TILE
	gfx.rect_fill( tx, ty, TILE, TILE )

	png.draw( 0, 0 ) 
	gfx.rect( 0, 0, png.width(), png.height() ) 

	map_grid()
	map_mini()
end

----------------------------------------------------
function map_grid()
	local maxw = map_iw -1 
	local maxh = map_ih -1
	local ts = TILE * map_scale
	local offx = png.width()
	for yi=0,maxh do 
		for xi=0,maxw do 
			gfx.rect( offx + xi*ts, yi*ts, ts, ts ) 
		end
	end		
end

function map_mini()
	local mult = 4 
	local px = 20 
	local py = TILE * 4 + 20 

	gfx.color( 255, 0, 0, 255)
	gfx.rect( px, py, mult * cells_w, mult * cells_h )
	gfx.rect_fill( px + map_ix*mult, py + map_iy*mult, 
				mult * map_iw, mult * map_ih )

end

----------------------------------------------------
function check_tilecontrol()
	local mx = mouse.x()
	local my = mouse.y()
	if mx < png.width() and my < png.height() then
		tix = math.floor( mx / TILE ) 
		tiy = math.floor( my / TILE ) 
	end
end

function check_mapcontrol()
	local mx = mouse.x()
	local my = mouse.y()
	mx = mx - png.width()

	if mx > 0 and mx < map_w*map_scale and my < map_h*map_scale then
		mx = mx / map_scale
		my = my / map_scale
		
		local pix = math.floor( mx / TILE ) + map_ix
		local piy = math.floor( my / TILE ) + map_iy

		local tx = tix * TILE
		local ty = tiy * TILE
		local px = pix * TILE
		local py = piy * TILE

		data.cells[1 + piy*cells_w + pix].ix = tix 
		data.cells[1 + piy*cells_w + pix].iy = tiy

		update_map()
	end
end

----------------------------------------------------
function shortcuts()

	if key.down( key.up_arrow ) then 
		map_iy = map_iy - 1
		if map_iy < 0 then map_iy = 0 end
		update_map()
	elseif key.down( key.down_arrow ) then 
		map_iy = map_iy + 1
		local max = cells_h - map_ih 
		if map_iy >= max then map_iy = max end
		update_map()
	elseif key.down( key.left_arrow ) then 
		map_ix = map_iy - 1
		if map_ix < 0 then map_ix = 0 end
		update_map()
	elseif key.down( key.right_arrow ) then 
		map_ix = map_ix + 1
		local max = cells_w - map_iw 
		if map_ix >= max then map_ix = max end
		update_map()
	end
	
	if key.pressed( key.w ) then 
		tiy = tiy - 1
	elseif key.pressed( key.s ) then 
		tiy = tiy + 1
	elseif key.pressed( key.a ) then 
		tix = tix - 1
	elseif key.pressed( key.d ) then 
		tix = tix + 1 
	end

	if key.pressed( key.q ) then
		map_scale = map_scale - 0.1
		update_map()
	end
	if key.pressed( key.e ) then
		map_scale = map_scale + 0.1
		update_map()
	end

	if key.pressed( key.enter ) then
		local file = io.open(levelfile, "w")
		io.output( file ) 
		io.write( json.encode( data ) ) 
		io.close( file ) 
		print( "level saved to "..levelfile )
	end 
end
