# scriptsuite

Collection of scripted tools made with [nsketch](https://codeberg.org/nonmateria/nsketch) :
* `autoeye` : like `eye` but captures a frame each 1.5 seconds
* `cereal`: process the `input` folder with `process.frag`
* `clayscan` : tool to acquire clay stop motion drawing using the webcam
* `eye`: monochrome webcam viewer
* `framescan` : tool to acquire ink animation drawing using the webcam
* `glsl` : minimum lua script to livecode a shader passed as cli argument
* `glsl_gif` : as `glsl`, but renders to a gif with some given arguments
* `glsl_cam` : as `glsl`, but with webcam input 
* `glsl_drop` : as `glsl`, but with test png frames drawn upon the layer 
* `tilecutter` : tool to cut equally sized tiles from a single input image
* `zinecutter` : tool to cut pages for the "motore immoto" zine

For most of these tools some options are changed by editing the topmost declared values, and functions are controlled with keyboard keys ( listed before the options ).
