
require "strict"

--[[-- KEYS -----------------------
	space : start processing

	( this script process files 
	  from the input/ folder
	  to the output/ folder 
	  using the process.frag shader)
--]]-------------------------------

----------------------------------------------------
window.size( 480, 480 )
window.title( "cereal" )

print( "loading folders..." )

png.load( "input" )
png.select( "input" )
local index = png.size()

if png.size() == 0 then 
	print( "no files found in the input folder" )
	window.quit()
else
	window.size( png.width(), png.height() )
	frag.load( "process.frag" ) 
	layer.create( "def" );
end

local clock = 0

----------------------------------------------------
function loop()
	png.select( "input" )
	--png.align_corner()
	
   	layer.clear( 0, 0, 0, 0 )
   	png.color( 255, 255, 255, 255 )
	png.draw( 0, 0 )

   	frag.apply( "process" )

	if index == png.size() then
		if clock == 0 then 
			png.next()
			clock = 10
		end
		clock = clock - 1 
	else
		local filename = "output/frame_"..tostring(index)..".png"
		layer.save( filename )	
		index = index+1 
		png.frame( index )
		print( "saving frame "..filename ) 
	end 

	if key.pressed( key.space ) then 
		index = 0
	end 
end
