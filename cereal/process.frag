
#ifdef GL_ES
precision highp float;
#endif

uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_tex_fb;

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution;
    
    vec4 source = texture2D( u_tex_fb, st ); // for texture access

	float a = 1.0 - source.r;

    gl_FragColor = vec4( vec3(1.0), a );
}
