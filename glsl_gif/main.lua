
require "strict"

----------------------------------------------------
local w = args.get_number( "-w", 480) 
local h = args.get_number( "-h", 480)
local m = args.get_number( "-m", 1)

if m < 1 then m = 1 end
window.size( w*m, h*m )

if args.count() > 0 then 
	-- TODO: compare arg to ".frag" here 
    frag.load( args.get(0), "glsl" ) 
	title =  args.get(1)
    window.title( title )
else
	window.quit()
end
  
layer.create( "def", w, h, m )
if args.get_bool( "--lin" ) then 
	layer.filter_linear()
end 

local px = args.get_number( "-x", -1)
local py = args.get_number( "-y", -1)

if px >= 0 and py >= 0 then 
	window.move( px, py )
end

local fps =  args.get_number( "-f", 60)
local sec =  args.get_number( "-s", 2)

local count = 0
local frames = sec * fps;
local step = 1.0 / frames

----------------------------------------------------
function loop()
	local t = count * step 
	clock.set( t ) 	

    frag.apply( "glsl" )

	if count < frames then	
		local filename = "out "
		if count < 10 then 
			filename = "output/frame_00"..tostring(count)..".png"
		elseif count < 100 then 
			filename = "output/frame_0"..tostring(count)..".png"
		else 
			filename = "output/frame_"..tostring(count)..".png"
		end
		 
		layer.save( filename )
	elseif count == frames then
		local delay = 100 / fps 
		io.popen( "mogrify -background black -alpha off output/* && convert -delay "..delay.." -loop 0 -dispose 2 output/*.png output/output.gif && rm output/*.png" )		
		print("generated gif in shader_gif/output" )
		
		os.exit()
	end

	count = count + 1
end
