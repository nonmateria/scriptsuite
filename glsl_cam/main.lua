
require "strict"

----------------------------------------------------
local w = args.get_number( "-w", 640) 
local h = args.get_number( "-h", 480)
local m = args.get_number( "-m", 1)

if m < 1 then m = 1 end
window.size( w*m, h*m )

if args.count() > 0 then 
	-- TODO: compare arg to ".frag" here 
    frag.load( args.get(0), "glsl" ) 
	title =  args.get(1)
    window.title( title )
else
	window.quit()
end
  
layer.create( "def", w, h, m )
if args.get_bool( "--lin" ) then 
	layer.filter_linear()
end 

local px = args.get_number( "-x", -1)
local py = args.get_number( "-y", -1)

if px >= 0 and py >= 0 then 
	window.move( px, py )
end

local dev = args.get_string( "-d", "/dev/video0" )

frag.load( args.get(0), "glsl" )      

cam.open( dev, w, h )

-- variables --------
local numsaved = 0
local freeze = 0

----------------------------------------------------
function loop()
	freeze = freeze + 1
	if freeze > 0 then 
    	layer.clear(0,0,0,0)
		cam.draw( 0, 0 )

	   	frag.apply( "glsl" )
	end

	if key.pressed( key.space ) or key.pressed( key.s ) then 
		local filename = "shadercam_output_"..tostring(numsaved)..".png"
		layer.select("def")
		layer.save( filename )
		freeze = -40
		numsaved = numsaved + 1
		print( filename )
	end 
end
